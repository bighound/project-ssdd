#!/bin/bash

echo "###############################"
echo "#### Sistemas Distribuidos ####"
echo "###############################"
echo -e "\n"
echo "Authors: Enrique"
echo "         Víctor"
echo "         Felipe"
echo -e "\n"
echo "All rights reserved"
echo -e
git clone "https://gitlab.com/bighound/project-ssdd.git"
cd "project-ssdd"


bash -c 'cd ./grpc-ssdd; mvn clean install; mvn package; mvn install:install-file -Dfile=target/grpc-client-jar-with-dependencies.jar -DgroupId=es.um.ssdd.grpc -DartifactId=grpc-client -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar' 2>/dev/null
bash -c 'cd ./external-apirest; mvn clean install; mvn package' 2>/dev/null
bash -c 'cd ./internal-apirest; mvn clean install; mvn package' 2>/dev/null

sudo docker-compose up
