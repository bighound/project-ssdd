package es.um.ssdd.practica.database;

import es.um.ssdd.practica.model.VideoModel;

public interface VideoDAO {
    void createVideo(VideoModel video);
    void addImage(String idVideo, String idImage, byte[] image);
    void updateVideo(String idVideo, String prop, String value);
    String getStatus(String idVideo);
    VideoModel getVideo(String idVideo);
}
