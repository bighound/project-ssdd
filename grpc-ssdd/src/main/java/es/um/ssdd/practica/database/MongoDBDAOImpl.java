package es.um.ssdd.practica.database;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import es.um.ssdd.practica.ServiceServer;
import es.um.ssdd.practica.model.VideoModel;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class MongoDBDAOImpl implements VideoDAO {
    private static MongoDBDAOImpl instance = null;
    private MongoDatabase database = null;

    public static MongoDBDAOImpl getInstance(){
        if (instance == null)
            instance = new MongoDBDAOImpl();
        return instance;
    }

    private MongoDBDAOImpl() {
        String uri = String.format("mongodb://%s:%s@%s:%d", ServiceServer.getMdbUser(), ServiceServer.getMdbPass(), ServiceServer.getMdbHost(), ServiceServer.getMdbPort());
        MongoClient mongoClient = new MongoClient(new MongoClientURI(uri));
        database = mongoClient.getDatabase("ssdd_ddbb");
    }

    @Override
    public void createVideo(VideoModel video) {
        MongoCollection videoCollection = database.getCollection("videos");
        Document object = new Document()
                .append("_id", new ObjectId(video.getId()))
                .append("title", video.getVname())
                .append("path", video.getPath())
                .append("uploadedBy", video.getUploadedBy())
                .append("date",video.getDateUpload())
                .append("faces", 0)
                .append("views", 0);
        videoCollection.insertOne(object);
    }


    @Override
    public void addImage(String idVideo, String idImage, byte[] image) {
        MongoCollection videosCollection = database.getCollection("videos");
        Document doc = new Document()
                .append("_id", new ObjectId())
                .append("name", idImage)
                .append("content", image);
        videosCollection.findOneAndUpdate(Filters.eq("_id", new ObjectId(idVideo)), new Document("$push", new Document("images", doc)));
        videosCollection.findOneAndUpdate(Filters.eq("_id", new ObjectId(idVideo)), new Document("$inc", new Document("faces", 1)));
    }

    @Override
    public void updateVideo(String idVideo, String prop, String value) {
        MongoCollection videosCollection = database.getCollection("videos");

        videosCollection.findOneAndUpdate(Filters.eq("_id", new ObjectId(idVideo)), new Document("$set", new Document(prop, value)));
    }

    @Override
    public VideoModel getVideo(String idVideo) {
        MongoCollection videosCollection = database.getCollection("videos");
        videosCollection.find(new Document().append("_id", new ObjectId(idVideo)));
        return null;
    }

    @Override
    public String getStatus(String idVideo) {
        MongoCollection videosCollection = database.getCollection("videos");
        List<Document> result = new ArrayList<Document>();
        videosCollection.find(new Document().append("_id", new ObjectId(idVideo))).into(result);
        if (result.size() == 0 || result.get(0).get("status").equals("procesando"))
            return "procesando";
        return result.get(0).get("status").toString();
    }
}
