package es.um.ssdd.practica;

import com.google.protobuf.ByteString;
import es.um.ssdd.practica.videoservice.*;
import org.bson.types.ObjectId;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiceClient {
	private static ServiceClient instance = null;
	private boolean initialized = false;
	private final int SIZE_MSG = 4000000;
	
	private static final Logger logger = Logger.getLogger(ServiceClient.class.getName());

	private ManagedChannel channel = null;
	private VideoServiceGrpc.VideoServiceBlockingStub blockingStub = null;
	private VideoServiceGrpc.VideoServiceStub asyncStub = null;

	public static ServiceClient getInstance() {
		if (instance == null)
			instance = new ServiceClient();
		return instance;
	}

	public void init(String host, int port) {
		channel = ManagedChannelBuilder.forAddress(host, port)
				.usePlaintext(true)
				.build();
		blockingStub = VideoServiceGrpc.newBlockingStub(channel);
		asyncStub = VideoServiceGrpc.newStub(channel);
		initialized = true;
	}

	public void shutdown() throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}

	/**
	 * Obtener las imagenes dado un id (nombre: video.mp4) de video
	 * El video debe existir en el directorio /tmp/
	 * @param videoId vídeo del cual queremos obtener las imágenes
	 */
	public Map<String, byte[]> getVideoImages(String videoId) {
		Map<String, byte[]> resp = new HashMap<String, byte[]>();
		if (!checkInit()) {
			System.err.println("You must init the instance .init(host, port)");
			logger.warning("Instance not initiated");
			resp.put("status", "error".getBytes());
			return resp;
		}

		Iterator<ImageResponse> response;
		
		try {
			response = blockingStub.getImages(VideoSpec.newBuilder().setId(videoId).build());
		} catch (StatusRuntimeException e) {
			logger.warning("RPC failed: " + e.getStatus());
			resp.put("status", "error".getBytes());
		    return resp;
		}

		ImageResponse it = response.next();

		if (it.getId().equals("PARTIAL")) {
			logger.info("El servidor está procesando el vídeo");
			resp.put("status", "partial".getBytes());
			return resp;
		}

		// Obtener las respuestas generadas
		byte[] img = null;
		while (response.hasNext()) {
			it = response.next();
			img = it.getImage().getData().toByteArray();
			if (img != null) {
				resp.put(it.getImage().getId(), it.getImage().getData().toByteArray());
				logger.info(String.format("%s %s > id: %s, size: %d ", it.getId(), it.getMessage(), it.getImage().getId(), img.length));
			} else {
				logger.info(String.format("%s %s",it.getId(), it.getMessage()));
			}
		}
		return resp;
	}

	/**
	 * Enviar video al servidor
	 * @param videoPath
	 * @throws IOException
	 */
	public String uploadVideo(String videoPath, String fileName, String idUploader) throws IOException {
		if (!checkInit()) {
			System.err.println("You must init the instance .init(host, port)");
			logger.warning("Instance not initiated");
			return "Init error";
		}

		if (videoPath == null || videoPath.equals("")) {
			logger.log(Level.WARNING, "Debe especificar la ruta de video ");
		    return "";
		}
		
		File f = new File(videoPath);
		
		if (!checkVideo(f)) {
			logger.log(Level.WARNING, "Problema al acceder al archivo");
			return "";
		}
		
		final String[] id = new String[1];
		id[0] = (new ObjectId()).toString();
		// Stream
		try {
			final CountDownLatch finishLatch = new CountDownLatch(1);

			StreamObserver<UploadResponse> soUpload = new StreamObserver<UploadResponse>() {
				@Override
				public void onNext(UploadResponse response) {
					logger.info(String.format("%s %s", response.getCode(), response.getId()));
					//id[0] = response.getId();
				}

				@Override
				public void onError(Throwable t) {
					finishLatch.countDown();
				}

				@Override
				public void onCompleted() {
					logger.info("Subida completada.");
					finishLatch.countDown();
				}
			};

			StreamObserver<Video> so = asyncStub.uploadVideo(soUpload);

			int videoChunksCnt = splitVideo(f, SIZE_MSG);
			byte[] videoChunk;

			int offset = 0;
			int size = 0;
			Video request;

			for (int i = 0; i < videoChunksCnt; i++) {
				// Calcular el offset de cada trozo
				offset = i*SIZE_MSG;
				size = SIZE_MSG;
				// Ultima parte del fichero, el tama;o cambia
				if (i == videoChunksCnt-1) {
					size = Math.toIntExact(f.length()) % SIZE_MSG;
				}

				// Leer un trozo de fichero
				videoChunk = readChunk(f, offset, size);
				// Generar la petici'on
				request = Video.newBuilder()
						.setId(id[0])
						.setName(fileName)
						.setIdUploader(idUploader)
						.setTotalParts(videoChunksCnt)
						.setPart(Chunk.newBuilder().setIndex(i+1).setContent(ByteString.copyFrom(videoChunk)).build())
						.build();
				 so.onNext(request);
			}
			so.onCompleted();

			if (finishLatch.await(1, TimeUnit.SECONDS))
				logger.info("Received response.");
			else
				logger.info("Not received response!");

		} catch (StatusRuntimeException e) {
			logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
			return "";
		} catch (InterruptedException e) {
			logger.log(Level.WARNING, "RPC failed: {0}", e.getMessage());
			return "";
		}

		return id[0];
	}

	/**
	 * Obtener un fichero por completo
	 * @param path
	 * @return
	 * @throws IOException
	 */
	private byte[] getVideo(String path) throws IOException {
		byte[] buffer = null;

		File f = new File(path);
		
		if (!f.exists() || !f.isFile() || !f.canRead())
			throw new IOException("File doesnt exist or cannot be read.");
		
		InputStream inputStream = new FileInputStream(f);
			
		buffer = new byte[(int) f.length()];

		inputStream.read(buffer);
		
		inputStream.close();

		return buffer;
	}

	/**
	 * Comprobar si el fichero existe y puedo ser leido
	 * @param f
	 * @return
	 */
	private boolean checkVideo(File f) {
		return f.exists() && f.isFile() && f.canRead();
	}

	/**
	 * Obtener el total de partes en las que se divide el fichero
	 * con el tama;o de mensaje establecido
	 * @param f
	 * @param size
	 * @return
	 */
	private int splitVideo(File f, int size) {
		int parts = 0;
		
		parts += f.length() / size;
		parts += ((f.length() % size) < size) ? 1 : 0;

		parts = Math.toIntExact(parts);
		
		return parts;
	}

	/**
	 * Leer un trozo del fichero
	 * @param f
	 * @param offset
	 * @param blockSize
	 * @return
	 */
	private byte[] readChunk(File f, int offset, long blockSize) {
		byte[] buffer = new byte[Math.toIntExact(blockSize)];
		RandomAccessFile inputStream;
		try {
			inputStream = new RandomAccessFile(f,"r");
			// Mover a la posicion que toca
			inputStream.seek(offset);
			int r = inputStream.read(buffer);
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return buffer;
    }

    private boolean checkInit() {
		return initialized;
	}

	public static void main(String[] args) {}
}
