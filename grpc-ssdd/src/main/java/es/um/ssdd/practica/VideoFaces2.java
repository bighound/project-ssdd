package es.um.ssdd.practica;

import org.openimaj.image.FImage;
import org.openimaj.image.Image;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.image.colour.Transforms;
import org.openimaj.image.processing.face.detection.DetectedFace;
import org.openimaj.image.processing.face.detection.FaceDetector;
import org.openimaj.image.processing.face.detection.HaarCascadeDetector;
import org.openimaj.video.Video;
import org.openimaj.video.VideoDisplay;
import org.openimaj.video.VideoDisplay.EndAction;
import org.openimaj.video.VideoDisplayListener;
import org.openimaj.video.VideoPositionListener;
import org.openimaj.video.xuggle.XuggleVideo;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class VideoFaces2 implements VideoDisplayListener<MBFImage>, VideoPositionListener {
	private static final Logger logger = Logger.getLogger(VideoFaces2.class.getName());
	private VideoDisplay<MBFImage> display;

	private String path;
	private String id;

	private boolean completed = false;
    int cnt = 0;
    
    public VideoFaces2(String path, String id, String ext) {
		if (!path.endsWith(System.getProperty("file.separator")))
			path += System.getProperty("file.separator");

		this.id = id;
		this.path = path;

		Video<MBFImage> video = new XuggleVideo(new File(String.format("%s%s.%s", path, id, ext)));
		display = VideoDisplay.createOffscreenVideoDisplay(video);
    	display.addVideoListener(this);
    	display.addVideoPositionListener(this);
    	display.setEndAction(EndAction.CLOSE_AT_END);
    }

	@Override
	public void afterUpdate(VideoDisplay<MBFImage> display) {}

	@Override
	public void beforeUpdate(MBFImage frame) {
		FaceDetector<DetectedFace,FImage> fd = new HaarCascadeDetector(40);
	  	List<DetectedFace> faces = fd.detectFaces(Transforms.calculateIntensity(frame));
		for (DetectedFace face : faces ) {
		    frame.drawShape(face.getBounds(), RGBColour.RED);

			try {
				String filename = String.format("%s%s%s_%d.%s", path, System.getProperty("file.separator"), id, cnt++, "png");
				// Guardar la imagen en fichero
				ImageUtilities.write(frame.extractROI(face.getBounds()),
    							  new File(filename));

				// Leer la imagen guardada
				File imgPath = new File(filename);
				BufferedImage originalImage= ImageIO.read(imgPath);
				ByteArrayOutputStream baos=new ByteArrayOutputStream();
				ImageIO.write(originalImage, "png", baos );

				// Almacenar la imagen en la base de datos. feo hacerlo desde aqu'i? Puede ser, pero funciona
				ServiceServer.getMongoClient().addImage(id,id+"_"+cnt+".png", baos.toByteArray());
			} catch (IOException e) {
				e.printStackTrace();
			}
	  	}
	}
	
	@Override
	public void videoAtStart(VideoDisplay<? extends Image<?, ?>> vd) {}

	@Override
	public void videoAtEnd(VideoDisplay<? extends Image<?, ?>> vd) {
	  	logger.info("Procesamiento completado");
	  	display.close();
		ServiceServer.getMongoClient().updateVideo(id,"status", "completado");
    	completed = true;
	}

	public boolean isCompleted() {
		return completed;
	}

}
