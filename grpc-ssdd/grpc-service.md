# Servicio GRPC

### Generación de ejecutables
Para generar los .jar del cliente y del servidor, realizamos los siguientes pasos en la carpeta del proyecto:
1. Ejecutar ```mvn clean install```
2. Ejecutar ```mvn package```. Este comando nos generará en la carpeta **target**, un fichero .jar para el cliente del servicio con todas las dependencias necesarias, y otro .jar para el servidor, también incluye las dependencias.

### Ejecución
#### Preparación
Para el correcto funcionamiento del servidor es necesario establecer las siguientes variables de entorno:

| Nombre | Descripción |
| ------ | ------ |
| MDB_HOST | URL de la base de datos MongoDB |
| MDB_PORT | Puerto de MongoDB |
| MDB_USER | Usuario que realiza la conexión |
| MDB_PASS | Password del usuario |

Nuestro servidor GRPC escucha en el puerto 50051.
Para la parte del cliente necesitamos las anteriores variables más dos nuevas:

| Nombre | Descripción |
| ------ | ------ |
| GRPC_HOST | URL del servidor GRPC |
| GRPC_PORT | Puerto del servidor GRPC |