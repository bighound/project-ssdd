# Project SSDD


En el proyecto se ha desarrollado una aplicación distribuida que permite subir vídeos, y obtener imágenes con los rostros faciales que aparecen en ellos, así como estadísticas relacionadas con el proceso.

Se compone de varios contenedores Docker que ofrecen la funcionalidad de gestionar usuarios, estadísticas de acceso a los vídeos que se suben a la plataforma y extracción de caras de dichos vídeos. 

## Capas de la aplicación
La aplicación se compone de las siguientes capas:  
    1. **Frontend**: A través de un acceso Web con todas las comunicaciones seguras con SSL. Se ha implementado con ficheros HTML y JavaScript. Y la parte servidor, que comunica con el backend, con Node.js.  
    2. **Backend**: Formado por los servidores relativos a la vista y los servidores que implementan los servicios REST y gRPC en Java.  
    3. **Datos**: La capa de datos implementada con MongoDB para almacenar los datos de todos los servicios y usuarios.  
    
Cada servicio se instala en contenedores independientes con Docker. Para descargar el proyecto y ejecutarlo, contamos con el script **initialize.sh**. Este descarga el proyecto, genera los paquetes *.jar, .war* necesarios, y ejecuta el fichero **docker-compose.yml** para desplegar todos los servicios mediante Docker.  
  
**NOTA**: para la correcta ejecución de la aplicación es  necesario  establecer  las  variables  de  entorno  en  el  **ficherossdd.env**.
