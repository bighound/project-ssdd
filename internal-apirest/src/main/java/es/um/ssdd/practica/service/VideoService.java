package es.um.ssdd.practica.service;

import es.um.ssdd.practica.AppConfig;
import es.um.ssdd.practica.ServiceClient;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.logging.Logger;

@Path("/user")
public class VideoService {
    private ServiceClient serviceClient = null;
    private static Logger logger = Logger.getLogger(VideoService.class.getName());

    private void initSC() {
        serviceClient = ServiceClient.getInstance();
        serviceClient.init(AppConfig.getGrpcHost(), AppConfig.getGrpcPort());
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadVideo(@Context HttpServletRequest request,
                              @HeaderParam("User") String username,
                              @FormDataParam("file") InputStream fileInputStream,
                              @FormDataParam("file") FormDataContentDisposition fileMetaData) {

        initSC();
        if (serviceClient == null) {
            logger.warning("Service GRPC unavailable");
            return createRes(Response.Status.SERVICE_UNAVAILABLE,"Service GRPC unavailable");
        }
        // Obtener el nombre del fichero [0] y la extension [1]
        String[] fileData = APIUtils.extractFileData(fileMetaData.getFileName());
        String id = "";
        if (fileData == null) return createRes(Response.Status.BAD_REQUEST, "Check filename");

        try {
            id = APIUtils.writeTmpFile(fileData, fileInputStream);
        } catch (IOException e) {
            return createRes(Response.Status.INTERNAL_SERVER_ERROR, "Error uploading file. Please, try again");
        }
        if (id == null || id.equals(""))
            return createRes(Response.Status.INTERNAL_SERVER_ERROR, "Error uploading file. Please, try again");

        // Enviar el video al servidor grpc
        String res = "";
        try {
            String path = String.format("%s%s%s%s%s.%s", APIUtils.TMPPATH, System.getProperty("file.separator"), id, System.getProperty("file.separator"), id, fileData[1]);
            res = serviceClient.uploadVideo(path, fileMetaData.getFileName(), username);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (res == null || res.equals(""))
            return createRes(Response.Status.GATEWAY_TIMEOUT, "Error uploading file to grpc");

        // Actualizar la lista de videos del usuario
        AppConfig.getMongoClient().updateUser(username, res, fileMetaData.getFileName());

        // Devolver código 204
        Response.ResponseBuilder response = Response.status(Response.Status.ACCEPTED);
        response.location(URI.create("/video/" + res));
        response.type(MediaType.TEXT_PLAIN_TYPE);
        response.entity(res);
        return response.build();
    }

    private Response createRes(Response.StatusType status, String msg) {
        return Response.status(status).entity(msg).build();
    }
}
