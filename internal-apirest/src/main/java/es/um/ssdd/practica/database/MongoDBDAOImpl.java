package es.um.ssdd.practica.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import es.um.ssdd.practica.AppConfig;
import org.bson.Document;
import org.bson.types.ObjectId;

public class MongoDBDAOImpl implements UserDAO {
    private static es.um.ssdd.practica.database.MongoDBDAOImpl instance = null;
    private MongoDatabase database;

    private MongoClient mongoClient;

    public static es.um.ssdd.practica.database.MongoDBDAOImpl getInstance(){
        if (instance == null)
            instance = new es.um.ssdd.practica.database.MongoDBDAOImpl();
        return instance;
    }

    private MongoDBDAOImpl() {
        String uri = String.format("mongodb://%s:%s@%s:%d", AppConfig.getMdbUser(), AppConfig.getMdbPass(), AppConfig.getMdbHost(), AppConfig.getMdbPort());
        mongoClient = new MongoClient(new MongoClientURI(uri));
        database = mongoClient.getDatabase("ssdd_ddbb");
    }

    @Override
    public void updateUser(String username, String idVideo, String videoName) {
        MongoCollection usersCollection = database.getCollection("users");
        Document doc = new Document().append("_id", new ObjectId(idVideo)).append("name", videoName);

        usersCollection.findOneAndUpdate(Filters.eq("username", username), Updates.addToSet("videos", doc));
    }
}
