// Funcionalidades del Ecmascript 6
'use strict'

// Cargamos el módulo de mongoose para poder conectarnos a MongoDB
var mongoose = require('mongoose');
var https = require('https');
var fs = require('fs');

//Certificates for SSL
var privateKey = fs.readFileSync(__dirname + '/certs/selfsigned.key');
var certificate = fs.readFileSync(__dirname + '/certs/selfsigned.crt');
var options = {
  key: privateKey,
  cert: certificate
};

const mdb_user = process.env.MDB_USER;
const mdb_pass = process.env.MDB_PASS;
const mdb_host = process.env.MDB_HOST;
const mdb_port = process.env.MDB_PORT;

const MONGO_URL = `mongodb://${mdb_user}:${mdb_pass}@${mdb_host}:${mdb_port}/ssdd_ddbb?authSource=admin`

// *Cargamos el fichero app.js con la configuración de Express
var app = require('./app');

// Puerto donde ejecutamos nuestro servidor
var port = 3800

// Le indicamos a Mongoose que haremos la conexión con Promises
mongoose.Promise = global.Promise;

//Generate https server
var server = https.createServer(options, app);

// Nos conectamos a nuestra bbdd de mongoDB
mongoose.connect(MONGO_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {

        // Cuando se realiza la conexión, lanzamos este mensaje por consola
        console.log("La conexión a la base de datos se ha realizado correctamente")

        // CREAR EL SERVIDOR WEB CON NODEJS
        server.listen(port, () => {
            console.log("servidor funcionando.");
        });
    })
    // Si no se conecta correctamente escupimos el error
    .catch(err => console.log(err));
