'use strict';

var User = require('../models/user');
var crypto = require('crypto')
// Cargamos la librería para encriptar la contraseña
var bcrypt = require("bcrypt");
var path = require('path');

exports.postLogin = function (req, res) {
    var user = req.body.username
    var pass = req.body.password;
    
    if (user != undefined && pass != undefined) {
        User.find({"username":user}).exec(async function (err, userObj) {
            // Si la peticion tiene algun error...
            console.log(userObj);
            if (err) {
                console.log(err);
                res.status(500).send({
                    error: 'Request error'
                })
            } else if (!userObj || userObj === [] || userObj.length === 0) { // Si no encontramos el objeto
                res.status(404).send({
                    error: 'Not found'
                })
            } else if (! await bcrypt.compare(pass, userObj[0].password)) { // Si la password no coincide
                res.status(404).send({
                    error: 'Password is incorrect'
                })
            }else { // Creamos el token
                var petition = "http://localhost:8080";
                var url = req.originalUrl;
                var today = new Date();
                today.setMilliseconds(0);
                today.setSeconds(0);
                today.setMinutes(0);
                var date = today.toISOString();
                var pw = userObj[0].password;
                //console.log("URL: "+url+"\n"+"Date: "+date+"\n"+"Pass: "+pw);
                var token = crypto.createHash('md5')
                            .update(petition.concat(url, date, pw))
                            .digest("hex");
                
                userObj[0].password = undefined;
                userObj[0].__v = undefined;
				var id = userObj[0]._id;
				return res.status(200).send({
					//token,
                    id
                });
            }
        })
    } else {
        res.status(500).send({
            error: 'Request error'
        })
    }
}

exports.getLogin = function(req,res,next){
	res.sendFile(path.join(__dirname,'../views','login.html'));
}