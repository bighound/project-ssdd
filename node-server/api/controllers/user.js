'use strict'

// Cargamos los modelos para usarlos posteriormente
var User = require('../models/user');

// Cargamos la librería para encriptar la contraseña
var bcrypt = require("bcrypt");

var BCRYPT_SALT_ROUNDS = 12;

// Obtener un usuario dado un ID
exports.getUser = function (req, res) {
	console.log(req.params.id);
    User.findById(req.params.id).exec(function (err, userObj) {
        // Si la peticion tiene algun error...
        if (err) {
            console.log(err);
            res.status(500).send({
                error: 'Request error'
            })
        } else if (!userObj) { // Si no encontramos el objeto
            res.status(404).send({
                error: 'Not found'
            })
        } else { // Podemos modificar el objeto antes de devolverlo, por ejemplo, quitando elementos.
            console.log(userObj);
			userObj.password = undefined;
			var i;
			var videoNames = "";
			for (i = 0; i < userObj.videos.length; i++) {
				videoNames += " " + userObj.videos[i].name + " " + userObj.videos[i]._id;
			} 
			return res.render('home',{data:{username:userObj.username,videos:videoNames}},function(err,html){
				res.send(html);
			});
    
        }
    });
}

// Buscar un usuario por su nombre y luego registra al usuario si no existe
exports.registerUser = function (req, res) {
    var user = req.body.username;
    var pass = req.body.password;
    if (!(user && pass) || user.length === 0 || pass.length === 0) {
        return res.status(400).send({
            error: 'Missing fields'
        })
    }
    User.find({"username":user}).exec(async function(err, userObj){
        // Si la peticion tiene algun error...
        if (err) {
            console.log(err);
            res.status(500).send({
                error: 'Request error'
            })
        } else if (!userObj[0]) { // Si no encontramos el usuario, lo creamos
                req.body.password = await bcrypt.hash(req.body.password, BCRYPT_SALT_ROUNDS);
                User.create(req.body, function(err, newUser) {
                    if(err) {
                        return res.status(500).send({
                            body: req.body,
                            error: 'Server error'
                        });
                    } else {
                        res.status(201).send({
                            newUser
                        })
                    }
                })
        } else { // Podemos modificar el objeto antes de devolverlo, por ejemplo, quitando elementos.
            return res.status(400).send({
                error: 'User already exists'
            });
        }
    });
}