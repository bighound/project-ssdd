var express = require('express');
var router = express.Router();
var path = require('path');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname,'../views','index.html'));
});

router.get('/loginStyle.css', function(req, res, next) {
  res.sendFile(path.join(__dirname,'../public/stylesheets','loginStyle.css'));
});

router.post('/api', function(req,res,next){
  res.sendFile(path.join(__dirname,'../views','login.html'));
});

module.exports = router;
