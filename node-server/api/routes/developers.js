var express = require('express');
var router = express.Router();
var path = require('path');

/* GET developers page. */
router.get('/developers', function(req, res, next) {
  res.sendFile(path.join(__dirname,'../views','desarrolladores.html'));
});


module.exports = router;