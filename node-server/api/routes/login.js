'use strict'

// Cargamos el módulo de express para poder crear rutas
var express = require('express');

// Cargamos el controlador
var LoginController = require('../controllers/login');

// Llamamos al router
var router = express.Router();
var api = express.Router();

// Creamos una ruta para los métodos que tenemos en nuestros controladores
api.post('/login', LoginController.postLogin);
api.get('/login', LoginController.getLogin);

router.get('/loginStyle.css', function(req, res, next) {
  res.sendFile(path.join(__dirname,'../public/stylesheets','loginStyle.css'));
});

// Exportamos la configuración
module.exports = api;
