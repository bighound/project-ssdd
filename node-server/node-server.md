API Empleada para acceder al servicio REST 
------
Esta API permite utilizar el servicio de detección de caras a través de la aplicación web, todas las comunicaciones desde el cliente hasta el frontend son seguras con SSL. Además, permite acceder al servidor interno GRPC redireccionando las peticiones a dicho servidor.


## Pasos para el despliegue del servicio
1. Necesario tener ejecutado previamente el **servidor GRPC** y el **servidor Tomcat**.
2. Nos situamos donde esté el fichero docker-compose.yml y ejecutamos la siguiente orden:
> $ sudo docker-compose build && sudo docker-compose up

3. Una vez nos aparezca el mensaje de que el servidor está funcionando ya tendremos lista nuestra infraestructura

-----------
## Endpoints

### Registro de usuario
> POST ```/user```

#### Parámetros de la petición
Parámetros | Descripción
--- | --- 
username   | Nombre del usuario
password   | Contraseña del usuario

#### Respuesta
Código | Mensaje | Descripción
--- | --- | ---
201 | Registered  | El usuario ha sido registrado correctamente
400 | User already exists | El nombre del usuario ya existe
400 | Missing fields | Si falta algún campo
500 | Request error | Error en el formato de la petición

### Inicio de sesión
> POST ```/login```

#### Cabeceras de la petición
Cabecera | Valor | Descripción
--- | --- | ---
User | String | Nombre del usuario
Date | String |Fecha en formato ISO8601, segundos y milisegundos a 0 (:00.000)

#### Parámetros de la petición
Parámetros | Descripción
--- | --- 
username   | Nombre del usuario
password   | Contraseña del usuario

#### Respuesta
Código | Mensaje | Descripción
--- | --- | ---
200 | Logged  | Ha iniciado sesión correctamente
404 | Not found | El nombre de usuario no existe
404 | Password is incorrect | La contraseña es incorrecta
500 | Request error | Error en el formato de la petición

### Obtener un usuario
> GET ```/user/:id```

#### Parámetros de la petición
Parámetros | Descripción
--- | --- 
:id   | Identificador del usuario

#### Respuesta
Código | Mensaje | Descripción
--- | --- | ---
200 |  | Se devuelve la información del usuario renderizada en el html
404 | Not found | El nombre de usuario no existe
500 | Request error | Error en el formato de la petición

### Obtener todos los usuarios
> GET ```/users```

#### Cabeceras de la petición
Cabecera | Valor | Descripción
--- | --- | ---
User-ID | String | Identificador de un usuario

#### Respuesta
Código | Mensaje | Descripción
--- | --- | ---
200 |  | Se devuelve a todos los usuarios
404 | Not found | No se ha encontrado ningún usuario
500 | Request error | Error en el formato de la petición

### Subir vídeo
> POST ```/user/:id/video```

#### Cabeceras de la petición
Cabecera | Valor | Descripción
--- | --- | ---
Content-Type | multipart/form-data | Tipo de contenido incluido en la petición 

#### Parámetros de la petición
Parámetros | Descripción
--- | --- 
:id   | Identificador del usuario

#### Respuesta
Código | Mensaje | Descripción
--- | --- | ---
202 | {:id} | El vídeo ha sido subido correctamente.
400 | Bad headers  | Si falta alguna cabecera
400 | Check filename | Comprobar el fichero subido
401 | You dont have permission  | El usuario no tiene permisos para acceder al servicio (Comprobar token de cliente)
404 | Wrong user or password | Usuario o contraseña incorrecta
500 | Error uploading file. Please, try again | El servidor GRPC está fallando
503 | Service GRPC unavailable | El servidor GRPC no está disponible
504 | Error uploading file to grpc | El servidor GRPC ha fallado al subir el fichero 

### Subir vídeo al servidor interno
> POST ```/user/:id/video-internal```

#### Cabeceras de la petición
Cabecera | Valor | Descripción
--- | --- | ---
Content-Type | multipart/form-data | Tipo de contenido incluido en la petición 

#### Parámetros de la petición
Parámetros | Descripción
--- | --- 
:id   | Identificador del usuario

#### Respuesta
Código | Mensaje | Descripción
--- | --- | ---
202 | {:id} | El vídeo ha sido subido correctamente.
400 | Bad headers  | Si falta alguna cabecera
400 | Check filename | Comprobar el fichero subido
401 | You dont have permission  | El usuario no tiene permisos para acceder al servicio (Comprobar token de cliente)
404 | Wrong user or password | Usuario o contraseña incorrecta
500 | Error uploading file. Please, try again | El servidor GRPC está fallando
503 | Service GRPC unavailable | El servidor GRPC no está disponible
504 | Error uploading file to grpc | El servidor GRPC ha fallado al subir el fichero

### Obtener las imágenes del vídeo
> GET ```/user/:id/video/:idVideo```

#### Cabeceras de la petición
Cabecera | Valor | Descripción
--- | --- | ---
User | String | Nombre del usuario
Date | String |Fecha en formato ISO8601, segundos y milisegundos a 0 (:00.000)
Content-Type | application | application/

#### Parámetros de la petición
Parámetros | Descripción
--- | --- 
:id   | Identificador del usuario
:idVideo | Identificador del vídeo

#### Respuesta
Código | Mensaje | Descripción
--- | --- | ---
200 |  | Se devuelve todas las imágenes asociadas al vídeo
204 | The video is still processing. Try it again in other moment | El vídeo está procesándose y no se puede obtener las imágenes en ese momento
404 | Not found | No se ha encontrado ningún vídeo
500 | Request error | Error en el formato de la petición

### Borrar un vídeo
> POST ```/user/:id/video/delete```

#### Parámetros de la petición
Parámetros | Descripción
--- | --- 
:id   | Identificador del usuario

#### Respuesta
Código | Mensaje | Descripción
--- | --- | ---
200 |  | El vídeo se ha eliminado correctamente
404 | Not found | No se ha encontrado ningún vídeo
500 | Request error | Error en el formato de la petición

### Borrar una imagen de un vídeo
> POST ```/user/:id/video/:idVideo/delete```

#### Parámetros de la petición
Parámetros | Descripción
--- | --- 
:id   | Identificador del usuario
:idVideo | Identificador del vídeo

#### Respuesta
Código | Mensaje | Descripción
--- | --- | ---
200 |  | Se elimina la imagen solicitada
404 | Not found | No se ha encontrado ningún vídeo
500 | Request error | Error en el formato de la petición

