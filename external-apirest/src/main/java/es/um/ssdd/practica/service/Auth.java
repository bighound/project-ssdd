package es.um.ssdd.practica.service;

import es.um.ssdd.practica.AppConfig;
import es.um.ssdd.practica.model.User;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

@Path("/user/register")
public class Auth {

    private final Logger logger = Logger.getLogger(Auth.class .getName());

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response userRegister(MultivaluedMap<String, String> postData) {
        String username = "";
        String password = "";
        try {
            username = postData.get("username").get(0);
            password = postData.get("password").get(0);
        } catch (NullPointerException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        String msg = "Registered!";
        if (!AppConfig.getMongoClient().createUser(new User(username,password)))
            msg = "Username exists";
        logger.info(String.format("%s:%s", username, password));
        return Response.ok().entity(msg).build();
    }
}
