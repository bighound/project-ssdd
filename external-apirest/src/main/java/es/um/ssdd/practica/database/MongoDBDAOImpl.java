package es.um.ssdd.practica.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Updates;
import es.um.ssdd.practica.AppConfig;
import es.um.ssdd.practica.model.User;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MongoDBDAOImpl implements UserDAO {
    private static MongoDBDAOImpl instance = null;
    private MongoDatabase database = null;

    private MongoClient mongoClient;

    public static MongoDBDAOImpl getInstance(){
        if (instance == null)
            instance = new MongoDBDAOImpl();
        return instance;
    }

    private MongoDBDAOImpl() {
        String uri = String.format("mongodb://%s:%s@%s:%d", AppConfig.getMdbUser(), AppConfig.getMdbPass(), AppConfig.getMdbHost(), AppConfig.getMdbPort());
        mongoClient = new MongoClient(new MongoClientURI(uri));
        database = mongoClient.getDatabase("ssdd_ddbb");
    }

    @Override
    public boolean createUser(User user) {
        MongoCollection usersCollection =  database.getCollection("users");
        Document query = new Document().append("username", user.getUsername());
        List result = new ArrayList<>();
        usersCollection.find(query).limit(1).into(result);
        if (result.size() != 0)
            return false;
        Document object = new Document()
                .append("username", user.getUsername())
                .append("password", user.getPassword())
                .append("videos", Collections.singletonList(""));
        usersCollection.insertOne(object);
        return true;
    }

    @Override
    public User getUser(String username) {
        MongoCollection usersCollection = database.getCollection("users");
        Document query = new Document("username", username);
        List<Document> result = new ArrayList<Document>();
        usersCollection.find(query).projection(Projections.include("_id", "username", "password")).limit(1).into(result);
        if (result.size() == 0)
            return null;
        User user = new User();
        user.setUsername(result.get(0).getString("username"));
        user.setPassword(result.get(0).getString("password"));
        return user;
    }

    @Override
    public void updateUser(String username, String idVideo, String videoName) {
        MongoCollection usersCollection = database.getCollection("users");
        Document doc = new Document().append("_id", new ObjectId(idVideo)).append("name", videoName);

        usersCollection.findOneAndUpdate(Filters.eq("username", username), Updates.addToSet("videos", doc));
    }
}
