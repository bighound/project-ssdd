package es.um.ssdd.practica.service;

import es.um.ssdd.practica.AppConfig;
import es.um.ssdd.practica.ServiceClient;
import es.um.ssdd.practica.model.User;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Path("/")
public class VideoService {
    private ServiceClient serviceClient = null;
    private static Logger logger = Logger.getLogger(VideoService.class.getName());

    private void initSC() {
        serviceClient = ServiceClient.getInstance();
        serviceClient.init(AppConfig.getGrpcHost(), AppConfig.getGrpcPort());
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getImages(@Context HttpServletRequest request,
                             @Context HttpHeaders httpHeaders,
                             @PathParam("id") String id) {
        if (!APIUtils.checkHeaders(httpHeaders))
            return createRes(Response.Status.BAD_REQUEST,"Bad headers");

        User user = getUser(httpHeaders.getHeaderString("user"));
        // Comprobar que existe el usuario
        if (!checkUser(user))
            return wrongLogin(0);
        // COmprobar que el token es correcto
        if (!checkToken(request.getRequestURI(), user.getPassword(), httpHeaders.getHeaderString("auth-token")))
            return wrongLogin(1);

        initSC();
        if (serviceClient == null) {
            logger.warning("Service GRPC unavailable");
            return createRes(Response.Status.SERVICE_UNAVAILABLE,"Service GRPC unavailable");
        }

        Map<String, byte[]> x = (HashMap<String, byte[]>) serviceClient.getVideoImages(id);
        if (x.containsKey("status")) {
            String status = new String(x.get("status"));
            // El servidor ha fallado
            if (status.equals("error"))
                return createRes(Response.Status.SERVICE_UNAVAILABLE,"Service GRPC unavailable");
            // El servidor está procesando la petición
            if (status.equals("partial"))
                return createRes(Response.Status.NO_CONTENT, "");
        }

        // Devolver las imágenes
        Response.ResponseBuilder response = Response.ok();
        response.header("Images", "OK;fileid=" + id);
        response.entity(x);
        return response.build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadVideo(@Context HttpServletRequest request,
                              @Context HttpHeaders httpHeaders,
                              @FormDataParam("file") InputStream fileInputStream,
                              @FormDataParam("file") FormDataContentDisposition fileMetaData) {
        // Comprobar que están todas cabeceras necesarias
        if (!APIUtils.checkHeaders(httpHeaders))
            return createRes(Response.Status.BAD_REQUEST,"Bad headers");

        User user = getUser(httpHeaders.getHeaderString("user"));
        // Comprobar que existe el usuario
        if (!checkUser(user))
            return wrongLogin(0);
        // COmprobar que el token es correcto
        if (!checkToken(request.getRequestURI(), user.getPassword(), httpHeaders.getHeaderString("auth-token")))
            return wrongLogin(1);

        initSC();
        if (serviceClient == null) {
            logger.warning("Service GRPC unavailable");
            return createRes(Response.Status.SERVICE_UNAVAILABLE,"Service GRPC unavailable");
        }
        // Obtener el nombre del fichero [0] y la extension [1]
        String[] fileData = APIUtils.extractFileData(fileMetaData.getFileName());
        String id = "";
        if (fileData == null) return createRes(Response.Status.BAD_REQUEST, "Check filename");

        try {
            id = APIUtils.writeTmpFile(fileData, fileInputStream);
        } catch (IOException e) {
            return createRes(Response.Status.INTERNAL_SERVER_ERROR, "Error uploading file. Please, try again");
        }
        if (id == null || id.equals(""))
            return createRes(Response.Status.INTERNAL_SERVER_ERROR, "Error uploading file. Please, try again");

        // Enviar el video al servidor grpc
        String res = "";
        try {
            String path = String.format("%s%s%s%s%s.%s", APIUtils.TMPPATH, System.getProperty("file.separator"), id, System.getProperty("file.separator"), id, fileData[1]);
            res = serviceClient.uploadVideo(path, fileMetaData.getFileName(), user.getUsername());
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (res == null || res.equals(""))
            return createRes(Response.Status.GATEWAY_TIMEOUT, "Error uploading file to grpc");

        // Actualizar la lista de videos del usuario
        AppConfig.getMongoClient().updateUser(user.getUsername(), res,fileMetaData.getFileName());

        // Devolver código 204
        Response.ResponseBuilder response = Response.status(Response.Status.ACCEPTED);
        response.location(URI.create("/video/" + res));
        response.type(MediaType.TEXT_PLAIN_TYPE);
        response.entity(res);
        return response.build();
    }

    private Response createRes(Response.StatusType status, String msg) {
        return Response.status(status).entity(msg).build();
    }

    private boolean checkUser(User user) {
        return user != null;
    }

    private boolean checkToken(String url, String password, String userToken) {
        return APIUtils.generateToken(url, password).equals(userToken);
    }

    private Response wrongLogin(int code) {
        if (code == 0)
            return createRes(Response.Status.NOT_FOUND, "Wrong user or password");
        if (code == 1)
            return createRes(Response.Status.UNAUTHORIZED, "You don't have permission");
        return null;
    }

    private User getUser(String username) {
        return AppConfig.getMongoClient().getUser(username);
    }
}
